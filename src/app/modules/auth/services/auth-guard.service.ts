import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { map, take } from "rxjs/operators";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";

import { AuthService } from "./auth.service";

@Injectable({ providedIn: 'root' })
export class AuthGuardService implements CanActivate {
  constructor(private _authService: AuthService,
              private _router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot):
    Observable<boolean | UrlTree> |
    Promise<boolean | UrlTree> |
    boolean | UrlTree {
    return this._authService.user.pipe(
      take(1),
      map(user => {
        const isAuthenticated = !!user;
        if (isAuthenticated) {
          return true;
        }
        return this._router.createUrlTree(['/auth']);
      })
    );
  }
}
