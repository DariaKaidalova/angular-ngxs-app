import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { catchError, tap } from "rxjs/operators";
import { BehaviorSubject, throwError } from "rxjs";

import { environment } from "../../../../environments/environment"
import { AuthInputDataInterface } from "../../../model/auth-input-data.interface";
import { AuthResponseDataInterface } from "../../../model/auth-response-data.interface";
import { UserDataInterface } from "../../../model/user-data.interface";
import { UserModel } from "../../../model/user.model";

@Injectable({ providedIn: 'root' })
export class AuthService {
  private _authUrl: string = 'https://identitytoolkit.googleapis.com/v1/accounts';
  private _tokenExpirationTimer: any;
  user = new BehaviorSubject<UserModel | null>(null);

  constructor(private _http: HttpClient,
              private _router: Router) {
  }

  signUp(signUpData: AuthInputDataInterface) {
    return this._http.post<AuthResponseDataInterface>(
      `${this._authUrl}:signUp?key=${environment.firebaseApiKey}`, {
        email: signUpData.email,
        password: signUpData.password,
        returnSecureToken: true
      }
    ).pipe(
      catchError(this.handleError),
      tap(responseData =>
        this.handleAuth(responseData.email, responseData.localId, responseData.idToken, responseData.expiresIn)
      )
    );
  }

  logIn(logInData: AuthInputDataInterface) {
    return this._http.post<AuthResponseDataInterface>(
      `${this._authUrl}:signInWithPassword?key=${environment.firebaseApiKey}`, {
        email: logInData.email,
        password: logInData.password,
        returnSecureToken: true
      }).pipe(
      catchError(this.handleError),
      tap(responseData =>
        this.handleAuth(responseData.email, responseData.localId, responseData.idToken, responseData.expiresIn)
      )
    );
  }

  logOut() {
    this.user.next(null);
    localStorage.removeItem('userData');
    if (this._tokenExpirationTimer) {
      clearTimeout(this._tokenExpirationTimer);
    }
    this._tokenExpirationTimer = null;
    this._router.navigate(['/auth']);
  }

  autoLogIn() {
    const userData: (string | null) = localStorage.getItem('userData')
    if (!userData) {
      return;
    }
    const parsedUserData: UserDataInterface = JSON.parse(userData);
    const tokenExpirationDate = new Date(parsedUserData._tokenExpirationDate);
    const loadedUser = new UserModel(
      parsedUserData.id,
      parsedUserData.email,
      parsedUserData._token,
      tokenExpirationDate
    );
    if (loadedUser.token) {
      this.user.next(loadedUser);
      const currentExpirationTime = tokenExpirationDate.getTime() - new Date().getTime()
      this.autoLogOut(currentExpirationTime);
    }
  }

  autoLogOut(expirationDuration: number) {
    this._tokenExpirationTimer = setTimeout(() => {
      this.logOut();
    }, expirationDuration);
  }

  private handleError(errorResponse: HttpErrorResponse) {
    let errorMessage = 'An unknown error occurred';
    if (!errorResponse.error || !errorResponse.error.error) {
      return throwError(errorMessage);
    }
    switch (errorResponse.error.error.message) {
      case 'EMAIL_EXISTS':
        errorMessage = 'The email address is already in use by another account.';
        break;
      case 'OPERATION_NOT_ALLOWED':
        errorMessage = 'Password sign-in is disabled for this project.';
        break;
      case 'TOO_MANY_ATTEMPTS_TRY_LATER':
        errorMessage = 'We have blocked all requests from this device due to unusual activity. Try again later.';
        break;
      case 'EMAIL_NOT_FOUND':
        errorMessage = 'There is no user record corresponding to this identifier. The user may have been deleted.';
        break;
      case 'INVALID_PASSWORD':
        errorMessage = 'The password is invalid or the user does not have a password.';
        break;
      case 'USER_DISABLED':
        errorMessage = 'The user account has been disabled by an administrator.';
        break;
    }
    return throwError(errorMessage);
  }

  private handleAuth(email: string, userId: string, token: string, expiresIn: string | number) {
    const currentExpirationTime = Number(expiresIn) * 1000;
    const expirationDate = new Date(new Date().getTime() + currentExpirationTime);
    const user = new UserModel(userId,
      email,
      token,
      expirationDate);
    this.user.next(user);
    this.autoLogOut(currentExpirationTime);
    localStorage.setItem('userData', JSON.stringify(user));
  }
}
