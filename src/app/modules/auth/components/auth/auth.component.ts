import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Observable } from "rxjs";
import { Router } from "@angular/router";

import { AuthResponseDataInterface } from "../../../../model/auth-response-data.interface";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit {
  isLoginMode: boolean = true;
  isLoading: boolean = false;
  error: string = '';

  constructor(private _authService: AuthService, private _router: Router) {
  }

  ngOnInit(): void {
  }

  onSwitchMode(form: NgForm) {
    this.isLoginMode = !this.isLoginMode;
    form.reset();
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }

    const signData = {
      email: form.value.email,
      password: form.value.password,
    }

    let authObservable: Observable<AuthResponseDataInterface>;

    this.isLoading = true;

    if (this.isLoginMode) {
      authObservable = this._authService.logIn(signData);
    } else {
      authObservable = this._authService.signUp(signData);
    }

    authObservable.subscribe(response => {
      this.isLoading = false;
      this._router.navigate(['/']);
    }, errorMessage => {
      console.error(errorMessage);
      this.error = errorMessage;
      this.isLoading = false;
    });
    form.reset();
  }
}
