import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { SharedModule } from "../shared/shared.module";
import { AuthRoutingModule } from "./auth.routing.module";

import { AuthPageComponent } from "./pages/auth-page/auth-page.component";
import { AuthComponent } from "./components/auth/auth.component";

@NgModule({
  declarations: [
    AuthPageComponent,
    AuthComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    AuthRoutingModule,
    SharedModule
  ],
  exports: []
})
export class AuthModule {
}
