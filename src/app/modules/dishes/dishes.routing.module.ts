import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

/* services */
import { AuthGuardService } from "../auth/services/auth-guard.service";

/* components */
import { DishesListPageComponent } from "./pages/dishes-list-page/dishes-list-page.component";
import { DishAddPageComponent } from "./pages/dish-add-page/dish-add-page.component";
import { DishDetailsPageComponent } from "./pages/dish-details-page/dish-details-page.component";
import { DishDetailsPageEditComponent } from "./pages/dish-details-page-edit/dish-details-page-edit.component";

const routes: Routes = [
  {
    path: '',
    component: DishesListPageComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'add',
    component: DishAddPageComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'details/:id',
    component: DishDetailsPageComponent,
    children: [
      {
        path: 'edit',
        component: DishDetailsPageEditComponent
      }
    ],
    canActivate: [AuthGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DishesRoutingModule {
}
