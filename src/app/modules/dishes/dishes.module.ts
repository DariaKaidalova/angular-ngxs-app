import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

/* modules */
import { SharedModule } from "../shared/shared.module";
import { DishesRoutingModule } from "./dishes.routing.module";

/* components */
import { DishesListPageComponent } from "./pages/dishes-list-page/dishes-list-page.component";
import { DishesListComponent } from "./components/dishes-list/dishes-list.component";
import { DishItemComponent } from "./components/dish-item/dish-item.component";

import { DishDetailsPageComponent } from "./pages/dish-details-page/dish-details-page.component";
import { DishDetailsComponent } from "./components/dish-details/dish-details.component";

import { DishDetailsPageEditComponent } from "./pages/dish-details-page-edit/dish-details-page-edit.component";
import { DishDetailsEditComponent } from "./components/dish-details-edit/dish-details-edit.component";

import { DishAddPageComponent } from "./pages/dish-add-page/dish-add-page.component";
import { DishAddComponent } from "./components/dish-add/dish-add.component";


@NgModule({
  declarations: [
    DishesListPageComponent,
    DishesListComponent,
    DishItemComponent,
    DishDetailsPageComponent,
    DishDetailsComponent,
    DishDetailsEditComponent,
    DishDetailsPageEditComponent,
    DishAddComponent,
    DishAddPageComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DishesRoutingModule,
    SharedModule
  ],
  exports: []
})
export class DishesModule {
}
