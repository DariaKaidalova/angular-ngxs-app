import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { Select, Store } from "@ngxs/store";
import { DishesState } from "../../../../store/states/dishes.state";
import { GetDishesList } from "../../../../store/actions/dishes.actions";

import { DishItemInterface } from "../../../../model/dish.interface";

@Component({
  selector: 'app-dishes-list-page',
  templateUrl: './dishes-list-page.component.html'
})
export class DishesListPageComponent implements OnInit {
  @Select(DishesState.getDishesList) dishesList: Observable<DishItemInterface[]>;

  constructor(private _store: Store) {
  }

  ngOnInit() {
    this._store.dispatch(new GetDishesList());
  }
}
