import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Observable } from "rxjs";
import { Select, Store } from "@ngxs/store";

import { DishesState } from "../../../../store/states/dishes.state";
import { EditDish, GetDish } from "../../../../store/actions/dishes.actions";

import { DishItemInterface } from "../../../../model/dish.interface";
import { DishSimpleItemInterface } from "../../../../model/dish-simple.interface";

@Component({
  selector: 'app-dish-details-page-edit',
  templateUrl: './dish-details-page-edit.component.html'
})
export class DishDetailsPageEditComponent implements OnInit {
  @Select(DishesState.getDishItem) dishItem: Observable<DishItemInterface>;
  id: string = ''

  constructor(private _route: ActivatedRoute,
              private _store: Store,
              private _router: Router) {
  }

  ngOnInit(): void {
    this.getDish();
  }

  getDish(): void {
    const parentParams = this._route.parent?.params;
    if (parentParams) {
      parentParams.subscribe(
        (params: Params | null | undefined) => {
          if (params) {
            this.id = params['id'];
          }
          this._store.dispatch(new GetDish(this.id));
        }
      );
    }
  }

  editDishItem(item: DishSimpleItemInterface): void {
    this._store.dispatch(new EditDish(this.id, item)).subscribe(() => {
      this._router.navigate(['/dishes']);
    });
  }
}
