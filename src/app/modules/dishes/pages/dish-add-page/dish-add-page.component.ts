import { Component, OnInit } from '@angular/core';
import { Select, Store } from "@ngxs/store";
import { Router } from "@angular/router";
import { Observable } from "rxjs";

import { AddDish } from "../../../../store/actions/dishes.actions";
import { DishesState } from "../../../../store/states/dishes.state";

import { DishSimpleItemInterface } from "../../../../model/dish-simple.interface";
import { DishItemInterface } from "../../../../model/dish.interface";

@Component({
  selector: 'app-dish-add-page',
  templateUrl: './dish-add-page.component.html'
})
export class DishAddPageComponent implements OnInit {
  @Select(DishesState.getDishesList) dishesList: Observable<DishItemInterface[]>;

  constructor(private _store: Store,
              private _router: Router) {
  }

  ngOnInit(): void {
    console.log('init add dish')
  }

  addDishItem(item: DishSimpleItemInterface): void {
    this._store.dispatch(new AddDish(item)).subscribe(() => {
      this._router.navigate(['/dishes']);
    });
  }
}
