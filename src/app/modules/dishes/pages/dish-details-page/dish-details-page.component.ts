import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Observable } from "rxjs";
import { Select, Store } from "@ngxs/store";

import { DishesState } from "../../../../store/states/dishes.state";
import { DeleteDish, GetDish, GetDishesList } from "../../../../store/actions/dishes.actions";

import { DishItemInterface } from "../../../../model/dish.interface";

@Component({
  selector: 'app-dish-details-page',
  templateUrl: './dish-details-page.component.html'
})
export class DishDetailsPageComponent implements OnInit {
  @Select(DishesState.getDishItem) dishItem: Observable<DishItemInterface>;
  id: string = '';

  constructor(private _route: ActivatedRoute,
              private _store: Store,
              private _router: Router) {
  }

  ngOnInit(): void {
    this.getDish();
  }

  getDish() {
    this._route.params.subscribe(
      (params: Params) => {
        this.id = params['id'];
        this._store.dispatch(new GetDish(this.id));
      }
    )
  }

  deleteDishItem(id: string): void {
    this._store.dispatch(new DeleteDish(id)).subscribe(
      () => this._store.dispatch(new GetDishesList())
    )
  }
}
