import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { AuthService } from "../../auth/services/auth.service";
import { DishItemInterface } from "../../../model/dish.interface";
import { DishSimpleItemInterface } from "../../../model/dish-simple.interface";

@Injectable({ providedIn: 'root' })
export class DishesRestService {
  private _urlFirebase: string = 'https://dishes-bdf4a-default-rtdb.firebaseio.com';

  constructor(private _http: HttpClient,
              private _authService: AuthService) {
  }

  getDishesList(): Observable<DishItemInterface[]> {
    return this._http.get(`${this._urlFirebase}/dishes.json`)
      .pipe(
        map(dishesObject => {
          const dishesArray = [];
          for (const key in dishesObject) {
            if (dishesObject.hasOwnProperty(key)) {
              dishesArray.push({ ...dishesObject[key as keyof typeof dishesObject], id: key } as DishItemInterface);
            }
          }
          return dishesArray;
        })
      );
  }

  getDishItem(id: string): Observable<DishSimpleItemInterface> {
    return this._http.get<DishSimpleItemInterface>(`${this._urlFirebase}/dishes/${id}.json`);
  }

  deleteDishItem(id: string): Observable<any> {
    return this._http.delete(`${this._urlFirebase}/dishes/${id}.json`);
  }

  addDishItem(dishItem: DishSimpleItemInterface): Observable<any> {
    return this._http.post(
      `${this._urlFirebase}/dishes.json`,
      dishItem
    );
  }

  editDishItem(id: string, dishItem: DishSimpleItemInterface): Observable<any> {
    return this._http.patch(
      `${this._urlFirebase}/dishes/${id}/.json`,
      dishItem
    );
  }
}


