import { Component, EventEmitter, Input, OnInit, Output, } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs";

import { DishItemInterface } from "../../../../model/dish.interface";
import { DishSimpleItemInterface } from "../../../../model/dish-simple.interface";


@Component({
  selector: 'app-dish-details-edit',
  templateUrl: './dish-details-edit.component.html',
})
export class DishDetailsEditComponent implements OnInit {
  @Input() item: Observable<DishItemInterface | null>;
  @Output() editItem = new EventEmitter<DishSimpleItemInterface>();
  editForm: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.item.subscribe((item: DishItemInterface | null) =>
      this.editForm = new FormGroup({
        'name': new FormControl(item?.name, Validators.required),
        'recipe': new FormControl(item?.recipe, Validators.required)
      })
    );
  }

  onSubmit(): void {
    this.editItem.emit(this.editForm.value);
  }
}
