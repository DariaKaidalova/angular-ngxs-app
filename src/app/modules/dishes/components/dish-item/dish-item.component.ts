import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DishItemInterface } from "../../../../model/dish.interface";

@Component({
  selector: 'app-dish-item',
  templateUrl: './dish-item.component.html',
})
export class DishItemComponent implements OnInit {
  @Input() item: DishItemInterface = {
    id: "0",
    name: '',
    recipe: ''
  };

  constructor(private _router: Router, private _route: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

  goToDetailsPage(id: string): void {
    this._router.navigate(['/dishes/details', id]);
  }
}
