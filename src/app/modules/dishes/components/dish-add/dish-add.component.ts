import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";

import { DishItemInterface } from "../../../../model/dish.interface";

@Component({
  selector: 'app-dish-add',
  templateUrl: './dish-add.component.html',
})
export class DishAddComponent implements OnInit {
  @Output() addItem = new EventEmitter<DishItemInterface>();
  addForm: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.addForm = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'recipe': new FormControl(null, Validators.required)
    });
  }

  onSubmit(): void {
    this.addItem.emit(this.addForm.value);
    this.addForm.reset();
  }
}
