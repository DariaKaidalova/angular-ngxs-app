import { Component, Input, OnInit } from '@angular/core';

import { DishItemInterface } from "../../../../model/dish.interface";

@Component({
  selector: 'app-dishes-list',
  templateUrl: './dishes-list.component.html',
})
export class DishesListComponent implements OnInit {
  @Input() list: DishItemInterface[] = [];

  constructor() {
  }

  ngOnInit() {
  }
}
