import {
  Component,
  ComponentFactoryResolver,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { DishSimpleItemInterface } from "../../../../model/dish-simple.interface";
import { PopupComponent } from "../../../shared/components/popup/popup.component";
import { PlaceholderDirective } from "../../../shared/directives/placeholder.directive";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-dish-details',
  templateUrl: './dish-details.component.html',
})
export class DishDetailsComponent implements OnInit, OnDestroy {
  private _cancelSubscription: Subscription;
  private _confirmSubscription: Subscription;
  @ViewChild(PlaceholderDirective, { static: false }) popupHost: PlaceholderDirective;
  @Input() id: string = '';
  @Input() item: DishSimpleItemInterface | null = {
    name: '',
    recipe: ''
  };
  @Output() deleteItem = new EventEmitter<string>();


  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    if (this._cancelSubscription) {
      this._cancelSubscription.unsubscribe();
    }
    if (this._confirmSubscription) {
      this._confirmSubscription.unsubscribe();
    }
  }

  editDish() {
    this._router.navigate(['edit'], { relativeTo: this._route });
  }

  deleteDishItem(): void {
    this.showPopup($localize`Do you really want to delete this dish?`);
  }

  showPopup(message: string = '') {
    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(PopupComponent);
    const hostRef = this.popupHost.viewContainreRef;
    hostRef.clear();
    const componentRef = hostRef.createComponent(componentFactory);
    componentRef.instance.content = message;
    this._cancelSubscription = componentRef.instance.cancelAction.subscribe(() => {
      this._cancelSubscription.unsubscribe();
      hostRef.clear();
    });
    this._confirmSubscription = componentRef.instance.confirmAction.subscribe(() => {
      this.deleteItem.emit(this.id);
      this._confirmSubscription.unsubscribe();
      hostRef.clear();
      this._router.navigate(['/dishes']);
    });
  }
}
