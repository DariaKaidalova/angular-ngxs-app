import { NgModule } from "@angular/core";

import { PopupComponent } from "./components/popup/popup.component";
import { LoadingComponent } from "../../components/loading/loading.component";

import { HighlightDirective } from "./directives/highlight.directive";
import { HighlightSimpleDirective } from "./directives/highlight-simple.directive";
import { UnlessDirective } from "./directives/unless.directive";
import { ToggleDirective } from "./directives/toggle.directive";
import { PlaceholderDirective } from "./directives/placeholder.directive";

@NgModule({
  declarations: [
    PopupComponent,
    LoadingComponent,
    HighlightDirective,
    HighlightSimpleDirective,
    UnlessDirective,
    ToggleDirective,
    PlaceholderDirective,
  ],
  imports: [],
  exports: [
    PopupComponent,
    HighlightDirective,
    HighlightSimpleDirective,
    UnlessDirective,
    ToggleDirective,
    PlaceholderDirective,
    LoadingComponent,
  ]
})
export class SharedModule {
}
