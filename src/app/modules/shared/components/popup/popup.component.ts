import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html'
})
export class PopupComponent implements OnInit {
  @Input() content: string = '';
  @Input() confirmText: string = $localize`Ok`;
  @Input() cancelText: string = $localize`Cancel`;
  @Output() confirmAction = new EventEmitter<void>();
  @Output() cancelAction = new EventEmitter<void>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.confirmAction.emit();
  }

  onCancel() {
    this.cancelAction.emit();
  }
}
