import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHighlightSimple]'
})
export class HighlightSimpleDirective {
  // elementRef is ref to the element where the directive was placed on
  // class Renderer is used to change the style of a HTML element. Use it for any DOM manipulations.
  constructor(private _elementRef: ElementRef, private _renderer: Renderer2) {
  }

  // @HostListener decorator declares a DOM event to listen for, and provides a handler method to run when that event occurs.
  @HostListener('mouseenter') mouseover(eventData: Event) {
    // styles changing via setStyle method https://angular.io/api/core/Renderer2#description
    this._renderer.setStyle(this._elementRef.nativeElement, 'color', 'gray');
  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this._renderer.setStyle(this._elementRef.nativeElement, 'color', 'initial');
  }
}
