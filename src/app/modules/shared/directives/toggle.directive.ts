import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appToggle]'
})
export class ToggleDirective {
  @HostBinding('class.-state_opened') isOpened = false;

  @HostListener('click') toggleByClick() {
    this.isOpened = !this.isOpened;
  }
}
