import { Directive, HostBinding, HostListener, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit {
  @Input() defaultColor: string = 'initial';
  @Input() highlightColor: string = 'gray';
  // Decorator that marks a DOM property as a host-binding property and supplies configuration metadata.
  // Angular automatically checks host property bindings during change detection,
  // and if a binding changes it updates the host element of the directive
  @HostBinding('style.color') color: string;

  constructor() {
  }

  // @HostListener decorator declares a DOM event to listen for, and provides a handler method to run when that event occurs.
  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.color = this.highlightColor;
  }

  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.color = this.defaultColor;
  }

  ngOnInit() {
    this.color = this.defaultColor;
  }
}
