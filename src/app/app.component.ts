import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from "./modules/auth/services/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private _authService: AuthService) {
  }

  ngOnInit() {
    this._authService.autoLogIn();
  }
}
