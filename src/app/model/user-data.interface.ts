export interface UserDataInterface {
  id: string,
  email: string,
  _token: string,
  _tokenExpirationDate: Date
}
