export interface DishItemInterface {
  id: string;
  name: string;
  recipe: string;
}
