export interface DishSimpleItemInterface {
  name: string;
  recipe: string;
}
