import { DishSimpleItemInterface } from "../../model/dish-simple.interface";

export class GetDishesList {
  static readonly type = '[Dishes List] Get';
}

export class GetDish {
  static readonly type = '[Dish Item] Get';

  constructor(public id: string) {
  }
}

export class DeleteDish {
  static readonly type = '[Dish Item] Delete';

  constructor(public id: string) {
  }
}

export class AddDish {
  static readonly type = '[Dish Item] Add';

  constructor(public item: DishSimpleItemInterface) {
  }
}

export class EditDish {
  static readonly type = '[Dish Item] Edit';

  constructor(public id: string,
              public item: DishSimpleItemInterface) {
  }
}
