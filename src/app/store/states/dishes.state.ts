import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Action, Selector, State, StateContext } from "@ngxs/store";
import { append, patch, removeItem, updateItem } from "@ngxs/store/operators";

import { DishItemInterface } from "../../model/dish.interface";
import { DishSimpleItemInterface } from "../../model/dish-simple.interface";
import { DishesRestService } from "../../modules/dishes/services/dishes-rest.service";
import { AddDish, DeleteDish, EditDish, GetDish, GetDishesList } from "../actions/dishes.actions";

export class DishesStateModel {
  dishesList: DishItemInterface[];
  dishItem: DishSimpleItemInterface | null;
}

@State<DishesStateModel>({
  name: 'dishes',
  defaults: {
    dishesList: [],
    dishItem: null
  }
})
@Injectable()
export class DishesState {
  constructor(private _storageService: DishesRestService) {
  }

  @Selector()
  static getDishesList(state: DishesStateModel) {
    return state.dishesList;
  }

  @Selector()
  static getDishItem(state: DishesStateModel) {
    return state.dishItem;
  }

  @Action(GetDishesList)
  getDishesList({ getState, setState }: StateContext<DishesStateModel>) {
    this._storageService.getDishesList()
      .subscribe((result) => {
        const state = getState();
        setState({
          ...state,
          dishesList: result,
        });
      });
  }

  @Action(GetDish)
  getDish({ getState, setState }: StateContext<DishesStateModel>, { id }: GetDish) {
    const state = getState();
    const possibleDish = state.dishesList.find(dish => dish.id === id);
    let dishObservable: Observable<DishSimpleItemInterface>;
    if (possibleDish) {
      dishObservable = of(possibleDish);
    } else {
      dishObservable = this._storageService.getDishItem(id)
    }

    dishObservable.subscribe((result) => {
      setState({
        ...state,
        dishItem: result,
      });
    });
  }

  @Action(DeleteDish)
  deleteDishItem({ getState, setState }: StateContext<DishesStateModel>, { id }: DeleteDish) {
    this._storageService.deleteDishItem(id)
      .subscribe(() => {
        setState(
          patch({
            dishesList: removeItem<any>(item => item.id === id),
          })
        );
      });

  }

  @Action(AddDish)
  addDishItem({ getState, setState }: StateContext<DishesStateModel>, { item }: AddDish) {
    this._storageService.addDishItem(item)
      .subscribe((result) => {
        setState(
          patch({
            dishesList: append<any>([{ ...item, id: result.name }]),
          })
        );
      });
  }

  @Action(EditDish)
  editDishItem({ getState, setState }: StateContext<DishesStateModel>, { id, item }: EditDish) {
    this._storageService.editDishItem(id, item)
      .subscribe(() => {
        setState(
          patch({
            dishesList: updateItem<any>(
              i => i.id === id,
              (i: DishItemInterface) => ({ ...item, id })),
          })
        );
      });
  }
}
