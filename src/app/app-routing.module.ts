import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from "./pages/home-page/home-page.component";
import { NotFoundPageComponent } from "./pages/not-found-page/not-found-page.component";

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'dishes', loadChildren: () => import('./modules/dishes/dishes.module').then(module => module.DishesModule) },
  { path: 'auth', loadChildren: () => import('./modules/auth/auth.module').then(module => module.AuthModule) },
  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { preloadingStrategy: PreloadAllModules }
    ),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
