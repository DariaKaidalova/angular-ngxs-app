import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
})
export class NavigationComponent implements OnInit {
  @Input() isAuthenticated: boolean = false;

  constructor() {
  }

  ngOnInit(): void {
  }
}
