import { Component, Input, OnInit } from '@angular/core';

import { AuthService } from "../../modules/auth/services/auth.service";

@Component({
  selector: 'app-navigation-buttons',
  templateUrl: './navigation-buttons.component.html'
})
export class NavigationButtonsComponent implements OnInit {
  @Input() isAuthenticated: boolean = false;

  constructor(private _authService: AuthService) {
  }

  ngOnInit(): void {
  }

  logOut() {
    this._authService.logOut();
  }
}
