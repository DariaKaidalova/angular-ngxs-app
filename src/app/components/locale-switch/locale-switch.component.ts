import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-locale-switch',
  templateUrl: './locale-switch.component.html'
})
export class LocaleSwitchComponent implements OnInit {
  localesList = [
    { link: '/en-US/', label: 'en' },
    { link: '/pl/', label: 'pl' }
  ]

  constructor() {
  }

  ngOnInit(): void {
  }
}
