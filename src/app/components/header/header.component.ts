import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from "rxjs";

import { AuthService } from "../../modules/auth/services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {
  private userSubscription: Subscription;
  isAuthenticated: boolean = false;

  constructor(private _authService: AuthService) {
  }

  ngOnInit(): void {
    this.userSubscription = this._authService.user.subscribe(user => {
      this.isAuthenticated = !!user;
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }
}
