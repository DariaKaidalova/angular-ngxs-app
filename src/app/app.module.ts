import { NgModule } from '@angular/core';
import { environment } from "../environments/environment";
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";

// state
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from "@ngxs/devtools-plugin";
import { NgxsLoggerPluginModule } from "@ngxs/logger-plugin";
import { DishesState } from "./store/states/dishes.state";

// modules
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from "./modules/core.module";

// components
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';
import { HeaderComponent } from './components/header/header.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { NavigationButtonsComponent } from './components/navigation-buttons/navigation-buttons.component';
import { ContentComponent } from './components/content/content.component';
import { LocaleSwitchComponent } from './components/locale-switch/locale-switch.component';

@NgModule({
  declarations: [
    AppComponent,

    HomePageComponent,
    NotFoundPageComponent,

    HeaderComponent,
    NavigationComponent,
    ContentComponent,
    NavigationButtonsComponent,
    LocaleSwitchComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    NgxsModule.forRoot([
      DishesState
    ], {
      developmentMode: !environment.production
    }),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot(),

    AppRoutingModule,
    RouterModule,
    CoreModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
